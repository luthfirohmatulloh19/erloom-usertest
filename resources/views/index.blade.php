<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <link href="{{asset('css/app.css')}}" rel="stylesheet">
</head>

<body>
    <div class="container">

        <div class="row">
            <div class="col-12 py-3">
                <!-- Error messages -->
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <!-- End error messages -->

                <!-- Success messages -->
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{!! $message !!}</p>
                    </div>
                @endif
                <!-- End sucess messages -->
                <form action="{{route('store')}}" method="POST">
                    @csrf
                    <label>Name : </label> <input type="text" name="name" />
                    <button class="btn btn-primary" type="submit">Create</button>
                </form>

            </div>
        </div>
        <div class="row">
            <div class="col-12">

                <table class="table py-3">
                    <thead>
                        <tr>
                            <th scope="col">No. ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Parity (Odd/Even)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- LOGIC START -->
                        @if($users->count() == 0)
                        <tr>
                            <td colspan="3" class="text-center"> Tidak ada data.</td>
                        </tr>
                        @else
                        <!-- LOGIC END -->
                            @foreach($users as $no => $user)
                            <tr>
                                <td> {{ ($users->firstItem() + $no) - 1 }}</td>
                                <td> {{ $user->name }} </td>
                                <td> {{ $user->parity }} </td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                {{ $users->links() }}
            </div>
        </div>
    </div>
</body>

</html>
