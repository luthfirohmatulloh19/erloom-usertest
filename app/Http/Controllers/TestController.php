<?php

namespace App\Http\Controllers;

use App\UserTest;
use Illuminate\Http\Request;

class TestController extends Controller
{
    private $userTest;

    function __construct(UserTest $userTest){
        $this->userTest = $userTest;
    }

    public function index()
    {
        $users = $this->userTest->paginate(10);
        // return response()->json($users);
        return view('index', compact('users'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ],[
            'name.required' => 'Nama tidak boleh kosong.'
        ]);

        $user = new $this->userTest;

        //count number of users
        $number_of_users = $this->userTest->count();

        // determine odd or even
        $parity = ($number_of_users % 2 == 0) ? 'Even' : 'Odd';

        //create user
        $user->create([
            'name' => $request->name,
            'parity' => $parity
        ]);

        return redirect()->route('index')->with('success','Data <b>'.$request->name.'</b> berhasil ditambahkan.');
    }

}
